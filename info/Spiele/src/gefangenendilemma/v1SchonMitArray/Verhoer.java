package gefangenendilemma.v1SchonMitArray;

public class Verhoer {

	private static int RUNDEN=5000;
	private final static boolean VERBOSE=false;
	
	private SpielerTyp[] gefangene = {new Randomi()};
	
	public void auswerten(int punkte1, int punkte2){
		System.out.print("1: "+punkte1+"\t\t");
		System.out.print("2: "+punkte2+"\t\t");
		System.out.println();	
	}
	public void endergebnis(int punkte1, int punkte2) {
		System.out.println("Durschschnitt "+1+": "+punkte1/(double)RUNDEN);
		System.out.println("Durschschnitt "+2+": "+punkte2/(double)RUNDEN);
	}
	
	public void durchfuehren() {
		for (int i = 0; i < gefangene.length; i++) {
			for (int j = i+1; j < gefangene.length; j++) {
				SpielerTyp g1 = gefangene[i];
				SpielerTyp g2 = gefangene[j];
				System.out.println(g1.getClass().getSimpleName()+" vs "+g2.getClass().getSimpleName()+":");
				runde(g1, g2);
			}
		}
	}

	public void runde(SpielerTyp gefangene1, SpielerTyp gefangene2){
		int punkte1 = 0;
		int punkte2 = 0;
		boolean antworten1, antworten2;
		for (int runde=1; runde<=RUNDEN; runde++) {
			if (VERBOSE) System.out.println("RUNDE "+runde+" --------------------");
			gefangene1.nimmZugInfoPrae();
			gefangene2.nimmZugInfoPrae();
			antworten1 = gefangene1.gibGestaendnis();				
			antworten2 = gefangene2.gibGestaendnis();				
			if (antworten1 && antworten2){
				punkte1 = punkte1 + 4;
				punkte2 = punkte2 + 4;				
			}
			else if (!antworten1 && !antworten2){
				punkte1 = punkte1 + 2;
				punkte2 = punkte2 + 2;				
			}
			else if (antworten1 && !antworten2){
				punkte1 = punkte1 + 1;
				punkte2 = punkte2 + 6;				
			}
			else {
				punkte1 = punkte1 + 6;
				punkte2 = punkte2 + 1;		
			}
			gefangene1.nimmZugInfoPost(antworten2);
			gefangene2.nimmZugInfoPost(antworten1);
			
			if (VERBOSE) {
				System.out.print(" "+1+": "+antworten1+"\t");				
				System.out.print(" "+2+": "+antworten2+"\t");				
				System.out.println();
			}
			if (runde == RUNDEN) {
				this.endergebnis(punkte1, punkte2);
			}
		} 		
	}
	
}
