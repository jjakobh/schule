package gefangenendilemma.v0NochOhneArray;



public class Verhoer {

	private static int RUNDEN=Integer.MAX_VALUE;
	private final static boolean VERBOSE=true;
	
	private SpielerTyp gefangene1,gefangene2;
	int punkte1,punkte2;
	
	public Verhoer(){
		gefangene1 = new Randomi();
		gefangene2 = new Kek();
	}

	public void auswerten(){
		System.out.print(" "+1+": "+punkte1+"    ");
		System.out.print(" "+2+": "+punkte2+"    ");
		System.out.println();	
	}
			
	public void durchfuehren(){
		boolean antworten1, antworten2;
		for (int runde=1; runde<=RUNDEN; runde++) {
			if (VERBOSE) {
				System.out.println("RUNDE "+runde+" --------------------");
			}
			gefangene1.nimmZugInfoPrae();
			gefangene2.nimmZugInfoPrae();
			antworten1 = gefangene1.gibGestaendnis();				
			antworten2 = gefangene2.gibGestaendnis();				
			if (antworten1 & antworten2){
				punkte1 = punkte1 + 4;
				punkte2 = punkte2 + 4;				
			}
			else if (!antworten1 & !antworten2){
				punkte1 = punkte1 + 2;
				punkte2 = punkte2 + 2;				
			}
			else if (antworten1 & !antworten2){
				punkte1 = punkte1 + 1;
				punkte2 = punkte2 + 6;				
			}
			else {
				punkte1 = punkte1 + 6;
				punkte2 = punkte2 + 1;		
			}
			gefangene1.nimmZugInfoPost(antworten2);
			gefangene2.nimmZugInfoPost(antworten1);
			
			if (VERBOSE | (runde==RUNDEN)) {
				System.out.print(" "+1+": "+antworten1+"  ");				
				System.out.print(" "+2+": "+antworten2+"  ");				
				System.out.println();
				this.auswerten();	
			}
		}
		System.out.println("Durchschnitt 1: "+(punkte1/(float)RUNDEN));
		System.out.println("Durchschnitt 2: "+(punkte2/(float)RUNDEN));
	}
	
}
