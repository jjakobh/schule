package gefangenendilemma.v0NochOhneArray;

public class Maxi extends SpielerTyp {
	
	private boolean hasBeenBetrayed = false;
	private int i = 0;
	
	public void nimmZugInfoPost(boolean gegnerZug){
		if(gegnerZug){
			hasBeenBetrayed = true;
		}
	}
	
	public boolean gibGestaendnis(){
		if(!hasBeenBetrayed){
			return false;
		}else{
			i += 1;
			if(i == 3){
				i = 0;
				hasBeenBetrayed = false;
			}
			return true;
		}
	}
}
