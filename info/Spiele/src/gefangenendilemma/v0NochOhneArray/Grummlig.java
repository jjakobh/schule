package gefangenendilemma.v0NochOhneArray;

public class Grummlig extends SpielerTyp {
	
	private boolean hasBeenBetrayed = false;
		
	public void nimmZugInfoPost(boolean gegnerZug){
		if (gegnerZug){
			hasBeenBetrayed = true;
		}
		
	}
	
	public boolean gibGestaendnis(){
//		if (hasBeenBetrayed) {
//			return true;
//		}else{
//			return false;
//		}
		return hasBeenBetrayed;
	}
	
}
