package gefangenendilemma.v0NochOhneArray;

public class Kek extends SpielerTyp {
	
	int anzahlRunden = 0;
	float davonTrue = 0;
	
	public void nimmZugInfoPost(boolean gegnerZug) {
		if (gegnerZug){
			davonTrue++;
		}
	}
	
	public boolean gibGestaendnis() {
		anzahlRunden++;
		
		float durchschnitt = davonTrue/anzahlRunden;
		
		if (durchschnitt < 0.3){
			return true;
		}else if (durchschnitt < 0.6){
			return true;
		}else{
			return true;
		}
	}
}
 	