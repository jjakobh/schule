package maumau.v0;

import nrw.ComparableContent;

import java.security.InvalidParameterException;

public class Karte implements ComparableContent<Karte> {

	public static final String[] farben = { "Karo", "Herz", "Pik", "Kreuz" };

	// Karo, Herz, Pik, Kreuz
	private int farbe;
	// 7 8 9 10 11=B 12=D 13=K 14=A
	private int wert;

	public Karte(String farbe, int wert) {
		int f;
		switch (farbe) {
		case "":
			f = -1;
			break;
		case "Karo":
			f = 0;
			break;
		case "Herz":
			f = 1;
			break;
		case "Pik":
			f = 2;
			break;
		case "Kreuz":
			f = 3;
			break;
		default:
			throw new InvalidParameterException("Farbe has to be Karo/Herz/Pik/Kreuz");
		}
		this.farbe = f;
		this.wert = wert;
	}

	public String getFarbe() {
		return farben[farbe];
	}

	public int getFarbeIndex() {
		return farbe;
	}

	public int getWert() {
		return wert;
	}

	public String toString() {
		String w;
		switch (this.wert) {
		case 11:
			w = "Bube";
			break;
		case 12:
			w = "Dame";
			break;
		case 13:
			w = "Koenig";
			break;
		case 14:
			w = "Ass";
			break;
		default:
			w = String.valueOf(wert);
			break;
		}

		return String.format("%s %s", farben[farbe], w);
	}

	// -1 ^= LT
	// 0 ^= EQ
	// 1 ^= GT
	private int compare(Karte other) {
		if (other.getWert() == this.getWert()) {
			return new Integer(farbe).compareTo(other.getFarbeIndex());
		} else {
			return new Integer(wert).compareTo(other.wert);
		}
	}

	public boolean isGreater(Karte other) {
		return compare(other) == 1;
	}

	public boolean isEqual(Karte other) {
		return compare(other) == 0;
	}

	public boolean isLess(Karte other) {
		return compare(other) == -1;
	}
}
