package maumau.v0;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;

import nrw.Stack;

public class Spielleiter {

	// KONFIGURATION
	public static final int N_START_KARTEN = 6;
	private boolean VERBOSE;

	// KARTEN
	private ArrayList<Karte> karten;
	private Stack<Karte> stapel;

	// SPIELER
	private SpielerInfo[] spieler;
	
	// ZUSTAND
	private boolean skip_next;

	// KONSTRUKTOR
	public Spielleiter(SpielerInfo[] spieler, boolean verbose) {
		if (spieler.length * N_START_KARTEN >= 32) {
			throw new InvalidParameterException("Zu viele Spieler");
		}
		this.spieler = spieler;
		this.VERBOSE = verbose;
		clear();
	}
	
	private static String[] namen = {"Michael", "Mike", "Kyryllo", "Johann", "Furkan", "Sirius", "Maxi", "Remo", "Florian", "Robert", "Richard", "Lennard", "Karim", "Jonas", "Jannis", "Lennard WoKu", "Christian"};
	public Spielleiter(SpielerTyp[] spielerTypen, boolean verbose) {
		this.spieler = new SpielerInfo[spielerTypen.length];
		for(int i = 0; i < spielerTypen.length; i++) {
			String name = namen[(int)(Math.random()*namen.length)];
			spieler[i] = new SpielerInfo(spielerTypen[i], String.format("%s (%s)", name, spielerTypen[i].getClass().getSimpleName()));
		}
		this.VERBOSE = verbose;
		clear();
	}
	
	// RUN
	public String run() {
		bereiteSpielerVor();
		gibAus();

		stapel.push(gibKarte());

		int runde = 1;
		while (true) {
			if (VERBOSE) {
				System.out.format(" --- RUNDE %d ---\n", runde);
				System.out.println("Oben: " + stapel.top());
			}

			for (SpielerInfo sp : spieler) {
				handleSpieler(sp, true);
				if (sp.karten.isEmpty()) {
					if (VERBOSE) System.out.format("%s gewinnt!\n", sp.name);
					return sp.name;
				}
			}

			runde++;
		}
	}

	// CLEAR
	public void clear() {
		for(SpielerInfo s : spieler) {
			s.clear();
		}

		stapel = new Stack<Karte>();
		initShuffledDeck();
		
		skip_next = false;
	}

	// --- Methoden für Spielverlauf ---
	private void bereiteSpielerVor() {
		String[] spielerNamen = new String[spieler.length];
		for(int i = 0; i < spieler.length; i++) spielerNamen[i] = spieler[i].name;
		if(VERBOSE)System.out.format("Es spielen %s\n\n", String.join(" vs ", spielerNamen));
		for (SpielerInfo s : spieler) {
			s.bereiteSpielVor();
		}
	}
	private void initShuffledDeck() {
		karten = new ArrayList<Karte>(Karte.farben.length * 8);
		for (String farbe : Karte.farben) {
			for (int wert = 7; wert <= 14; wert++) {
				karten.add(new Karte(farbe, wert));
			}
		}
		Collections.shuffle(karten);
	}
	private void gibAus() {
		for (SpielerInfo sp : spieler) {
			for (int i = 0; i < N_START_KARTEN; i++) {
				sp.nimmKarte(gibKarte());
			}
		}
	}
	private void handleSpieler(SpielerInfo spieler, boolean canZiehAgain) {
		if (skip_next) {
			skip_next = false;
			if (VERBOSE) System.out.format("%s muss aussetzen\n", spieler.name);
			return;
		}

		Karte zug = spieler.gibZug(stapel.top());

		if (zug == null) {
			if(canZiehAgain) {
				if (VERBOSE) System.out.format("%s muss ziehen\n", spieler.name);
				spieler.nimmKarte(gibKarte());
				
				handleSpieler(spieler, false);
			} else {
				if (VERBOSE) System.out.format("%s kann immer noch nicht\n", spieler.name);
			}
			return;
		}
		
		stapel.push(zug);
		if (VERBOSE) System.out.format("%s legt %s\n", spieler.name, zug);
		
		if(zug.getWert() == 8) {
			skip_next = true;
		} else if (zug.getWert() == 11) {
			//String farbe = spieler[spielerIndex].waehleFarbe();
		}
	}

	// HILFSMETHODEN
	private Karte gibKarte() {
		if (karten.isEmpty()) {
			refill();
		}
		int lastIndex = karten.size() - 1;
		if(lastIndex < 0) {
			throw new Error("no card on give stack");
		}
		return karten.remove(lastIndex);
	}


	private void refill() {
		if(VERBOSE) System.out.println("REFILL");
		Karte top = stapel.top();
		stapel.pop();
		while (!stapel.isEmpty()) {
			karten.add(stapel.top());
			stapel.pop();
		}
		if (top != null) {
			stapel.push(top);
		}
		Collections.shuffle(karten);
	}
}
