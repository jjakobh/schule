package maumau.v0;

import java.util.HashMap;
import java.util.Map;

public class Main {
	
	public static final int ANZAHL_SPIELE = 100000;

	public static void main(String[] args) {
		spiel();
	}
	
	public static void spiel() {
		SpielerTyp[] spieler = { new Plajer(), new Randomi() };
		Spielleiter leiter = new Spielleiter(spieler, true);
		
		String gewinner = leiter.run();
		
		System.out.println();
		System.out.format("Gewinner: %s", gewinner);
	}
	
	
	
	
	public static void vergleicheSpieler() {
		SpielerInfo[] spieler = {
				new SpielerInfo(new Plajer(), "Plajer"),
				new SpielerInfo(new Randomi(), "Randomi")
		};
		Map<String, Integer> gewinner = new HashMap<String, Integer>();

		Spielleiter leiter = new Spielleiter(spieler, false);
		
		for(int i = 0; i < ANZAHL_SPIELE; i++) {
			String g = leiter.run();
			int count = gewinner.containsKey(g) ? gewinner.get(g) : 0;
			gewinner.put(g, count+1);
			leiter.clear();
			System.out.println(g);
		}
		
		for(String s : gewinner.keySet()) {
			System.out.format("%s: %.01f%% Gewinne\n", s, (gewinner.get(s)*100)/(float)ANZAHL_SPIELE);
		}
	}
	
	
	
	
	
	
	
	
	
	static void testKartenCompare() {
		Karte a = new Karte("Pik", 10);
		Karte b = new Karte("Pik", 8);
		Karte c = new Karte("Karo", 8);
		Karte zero = new Karte("Karo", 0);

		System.out.format("a cmp b    GT: %b\tEQ: %b\tLT: %b\n", a.isGreater(b), a.isEqual(b), a.isLess(b));
		System.out.format("c cmp b    GT: %b\tEQ: %b\tLT: %b\n", c.isGreater(b), c.isEqual(b), c.isLess(b));
		System.out.format("zero cmp c    GT: %b\tEQ: %b\tLT: %b\n", zero.isGreater(c), zero.isEqual(c), zero.isLess(c));
	}

	static void testSpielerZeug() {
		NaiveSpieler a = new NaiveSpieler();
		a.bereiteSpielVor();
		a.nimmKarte(new Karte("Karo", 8));
		a.nimmKarte(new Karte("Karo", 7));
		a.nimmKarte(new Karte("Kreuz", 11));
		a.nimmKarte(new Karte("Pik", 10));
		a.nimmKarte(new Karte("Pik", 9));

		Karte kek = a.gibFirstOfFarbe("Pik");
		System.out.println(kek);
	}

}
