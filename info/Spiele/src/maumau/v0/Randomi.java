package maumau.v0;

import java.util.ArrayList;

public class Randomi implements SpielerTyp {
	
	ArrayList<Karte> karten = new ArrayList<Karte>();

	@Override
	public void bereiteSpielVor() {
	}

	@Override
	public void nimmKarte(Karte pKarte) {
		karten.add(pKarte);
	}

	@Override
	public Karte gibZug(String farbe, int wert) {
		if(!karten.isEmpty()) {
			return karten.remove(karten.size()-1);
		}
		return null;
	}

	@Override
	public String waehleFarbe() {
		return null;
	}

}
