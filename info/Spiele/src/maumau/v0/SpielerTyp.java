package maumau.v0;

public interface SpielerTyp {
	public void bereiteSpielVor();

	public void nimmKarte(Karte pKarte);

	public Karte gibZug(String farbe, int wert);

	public String waehleFarbe();
}
