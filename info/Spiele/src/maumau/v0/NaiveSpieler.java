package maumau.v0;

import nrw.BinarySearchTree;

public class NaiveSpieler implements SpielerTyp {

	BinarySearchTree<Karte> karten;

	public void bereiteSpielVor() {
		karten = new BinarySearchTree<Karte>();
	}

	public void nimmKarte(Karte karte) {
		karten.insert(karte);
	}

	public Karte gibZug(String farbe, int wert) {
		Karte x = gibFirstOfFarbe(farbe);
		if (x != null) {
			return x;
		}
		return gibFirstOfWert(wert);
	}

	public String waehleFarbe() {
		return null;
	}

	// ---- Hilfsfunktionen ----
	Karte gibFirstOfFarbe(final String farbe) {
		BinarySearchTree<Karte> tmp = karten;
		Karte cmp = new Karte(farbe, 0);

		while (true) {
			if (tmp.isEmpty()) {
				return null;
			}

			Karte ye = tmp.getContent();
			if (ye.getFarbe() == farbe) {
				tmp.remove(ye);
				return ye;
			}
			if (cmp.isLess(ye)) {
				tmp = tmp.getLeftTree();
			} else {
				tmp = tmp.getRightTree();
			}
		}
	}

	Karte gibFirstOfWert(int wert) {
		BinarySearchTree<Karte> tmp = karten;

		Karte cmp = new Karte("", wert);

		while (true) {
			if (tmp.isEmpty()) {
				return null;
			}

			Karte ye = tmp.getContent();
			if (ye.getWert() == wert) {
				tmp.remove(ye);
				return ye;
			}
			if (cmp.isLess(ye)) {
				tmp = tmp.getLeftTree();
			} else {
				tmp = tmp.getRightTree();
			}
		}
	}
}
