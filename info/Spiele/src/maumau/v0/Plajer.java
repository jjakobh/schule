package maumau.v0;

import java.util.ArrayList;
import java.util.function.Predicate;

import nrw.BinarySearchTree;

public class Plajer implements SpielerTyp {
	
	private BinarySearchTree<Karte> karten;

	public void bereiteSpielVor() {
		karten = new BinarySearchTree<Karte>();
	}

	public void nimmKarte(Karte karte) {
		karten.insert(karte);
	}

	public Karte gibZug(final String farbe, final int wert) {
		Predicate<Karte> sameFarbe = new Predicate<Karte>() {
			public boolean test(Karte t) { return t.getFarbe().equals(farbe); }
		};
		Predicate<Karte> sameWert = new Predicate<Karte>() {
			public boolean test(Karte t) { return t.getWert() == wert; }
		};
		
		
		ArrayList<Karte> kartenBuffer = new ArrayList<Karte>();
		findKartenWhere(sameFarbe, karten, kartenBuffer);

		if(!kartenBuffer.isEmpty()) {
			Karte ka = kartenBuffer.get(0);
			karten.remove(ka);
			return ka;
		}
		
		kartenBuffer.clear();
		findKartenWhere(sameWert, karten, kartenBuffer);

		if(!kartenBuffer.isEmpty()) {
			Karte ka = kartenBuffer.get(0);
			karten.remove(ka);
			return ka;
		}

		return null;
	}

	public String waehleFarbe() {
		return null;
	}
	
	public void findKartenWhere(Predicate<Karte> pred, BinarySearchTree<Karte> karten, ArrayList<Karte> kartenOfFarbe) {
		if(!karten.isEmpty()) {
			if(pred.test(karten.getContent())) {
				kartenOfFarbe.add(karten.getContent());
			}
			
			findKartenWhere(pred, karten.getLeftTree(), kartenOfFarbe);
			findKartenWhere(pred, karten.getRightTree(), kartenOfFarbe);
		}
	}

}
