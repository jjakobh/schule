package maumau.v0;

import java.util.ArrayList;

public class SpielerInfo {
	private SpielerTyp spieler;
	ArrayList<Karte> karten;
	String name;
	
	public SpielerInfo(SpielerTyp spieler, String name) {
		this.spieler = spieler;
		this.karten = new ArrayList<>();
		this.name = name;
	}
	
	public void clear() {
		karten.clear();
	}
	
	public void bereiteSpielVor() {
		spieler.bereiteSpielVor();
	}

	public void nimmKarte(Karte karte) {
		karten.add(karte);
		spieler.nimmKarte(karte);
	}
	
	public Karte gibZug(Karte karte) {
		Karte k = spieler.gibZug(karte.getFarbe(), karte.getWert());
		if (k != null && karten.remove(k) == false) {
			System.out.format("%s ist ein Schummler!", name);
			System.exit(1);
		}
		return k;
	}
}
