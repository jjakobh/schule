package sort.compare;

public class Main {
	
	
	public static void moin(String[] args) {
		Zahlenspeicher s = new Zahlenspeicher(8, 100);
		s.print();
		System.out.println("-------------------");
		Zahlenspeicher.quicksort(s.zahlen, s.zahlen.length);
		s.print();
	}
	
	public static void main(String[] args) {
		final int m = 100;
		final int n = 1000;
		test(10, new Runnable() {
			public void run() {
				Zahlenspeicher s = new Zahlenspeicher(n, m);
				Zahlenspeicher.quicksort(s.zahlen, s.zahlen.length);
			}
		});
	}
	
	public static void test(int k, Runnable r) {
		long sum = 0;
		long min = Long.MAX_VALUE;
		long max = Long.MIN_VALUE;
		for (int i = 0; i < k; i++) {
			//Zahlenspeicher s = new Zahlenspeicher(n, m);
			long t0 = System.currentTimeMillis();
			//Zahlenspeicher.quicksort(s.zahlen, s.zahlen.length);
			r.run();
			long t1 = System.currentTimeMillis();
			long delta = t1-t0;

			sum += delta;
			if (delta < min)
				min = delta;
			if (delta > max)
				max = delta;
		}
		long avg = sum / (long) k;
		System.out.format("AVG: %d / MIN: %d / MAX: %d", avg, min, max);
	}

	public static void quicktest() {

		int k = 10;
		//int n = 1000000;
		int m = 1000;
		
		int reps = 6;
		
		long[] mins = new long[reps];
		long[] maxs = new long[reps];
		long[] avgs = new long[reps];
		
		//System.out.println("m\t\tAVG\tMIN\tMAX");
		for (int x = 0; x < reps; x++) {
			int n = (int) (50000 * Math.pow(2, x));
			long sum = 0;
			long min = Long.MAX_VALUE;
			long max = Long.MIN_VALUE;
			for (int i = 0; i < k; i++) {
				Zahlenspeicher s = new Zahlenspeicher(n, m);
				long t0 = System.currentTimeMillis();
				Zahlenspeicher.quicksort(s.zahlen, s.zahlen.length);
				long t1 = System.currentTimeMillis();
				long delta = t1-t0;
	
				sum += delta;
				if (delta < min)
					min = delta;
				if (delta > max)
					max = delta;
			}
			long avg = sum / (long) k;
			//System.out.format("%d\t\t%d\t%d\t%d\n", n, avg, min, max);
			avgs[x] = avg;
			mins[x] = min;
			maxs[x] = max;
		}
		
		
		System.out.println();
		System.out.println("m,avg,min,max");
		for (int i = 0; i < reps; i++) {
			System.out.format("%d,%d,%d,%d\n", (int) (50000 * Math.pow(2, i)), avgs[i], mins[i], maxs[i]);
		}
	}
	
	
}
