package sort.compare;

public class Zahlenspeicher {
	
	public int[] zahlen;
	
	public Zahlenspeicher(int n, int m) {
		zahlen = new int[n];
		for(int i = 0; i < n; i++) {
			zahlen[i] = (int) (Math.random()*m)+1;
		}
	}
	
	public void print() {
		for(int i = 0; i < zahlen.length; i++) {
			System.out.println(zahlen[i]);
		}
	}
	
	// SORTS
	public void sortSelection() {
		for(int i = 0; i < zahlen.length; i++) {
			swap(i, minIndex(i));
		}
	}
	
	public static void quicksort(int[] zahlen, int n) {
		int pivotIndex = (int) (Math.random()*n);
		int pivotWert = zahlen[pivotIndex];
		int[] links = new int[n];
		int[] rechts = new int[n];
		
		int l = 0, r = 0, m = 0;
		for(int i = 0; i < n; i++) {
			if(zahlen[i] < pivotWert) {
				links[l] = zahlen[i];
				l++;
			} else if (zahlen[i] > pivotWert) {
				rechts[r] = zahlen[i];
				r++;
			} else {
				m++;
			}
		}
		if (l > 0) {
			quicksort(links, l);
		}
		if (r > 0) {
			quicksort(rechts, r);
		}
		for (int i = 0; i < l; i++) {
			zahlen[i] = links[i];
		}
		zahlen[l] = pivotWert;
		for (int i = 0; i < r; i++) {
			zahlen[i+m+l] = rechts[i];
		}
	}
	
	// HELPERS
	public void swap(int indexA, int indexB) {
		int tmp = zahlen[indexB];
		zahlen[indexB] = zahlen[indexA];
		zahlen[indexA] = tmp;
	}
	
	public int minIndex(int start) {
		int max = Integer.MAX_VALUE;
		int index = -1;
		for(int i = start; i < zahlen.length; i++) {
			if(zahlen[i] < max) {
				max = zahlen[i];
				index = i;
			}
		}
		return index;
	}
	
}
