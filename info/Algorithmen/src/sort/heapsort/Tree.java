package sort.heapsort;

public class Tree<T> {
	
	private class TreeNode {
		TreeNode left;
		TreeNode right;
		
		T value;
		
		public TreeNode(T value) { this.value = value; }
	}
	
	private TreeNode root;

	public Tree() {}
	
	public Tree(T[] ts) {
		if(ts.length > 0) {
			root = insertInNode(ts, root, 0);
		}
	}
	
	public void print() {
		pprriinntt(root, "");
	}
	private void pprriinntt(TreeNode noot, String pre) {
		if(noot != null) {
			pprriinntt(noot.left, pre+"\t");
			System.out.println(pre+noot.value);
			pprriinntt(noot.right, pre+"\t");
		}
	}
	
	private TreeNode insertInNode(T[] array, TreeNode node, int i) {
		if (i < array.length) {
			node = new TreeNode(array[i]);
			
			node.left = insertInNode(array, node.left, 2*i+1);
			node.right = insertInNode(array, node.right, 2*i+2);
		}
		return node;
	}

}
