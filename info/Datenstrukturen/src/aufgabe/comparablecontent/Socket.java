package aufgabe.comparablecontent;

import java.util.regex.Pattern;
import nrw.ComparableContent;

public class Socket implements ComparableContent<Socket>{
	int[] parts = new int[4];
	int port;
	
	public Socket(String ip, int port) throws NumberFormatException {
		String[] stringParts = ip.split(Pattern.quote("."));
		if (stringParts.length != 4)
			throw new IllegalArgumentException("IP Addr should be in range 0-255");
		
		for(int i = 0; i < stringParts.length; i++) {
			this.parts[i] = Integer.parseInt(stringParts[i]);
		}
		
		this.port = port;
	}
	
	public String toString() {
		return String.format("%d.%d.%d.%d:%d", parts[0], parts[1], parts[2], parts[3], port);
	}

	
	private int compare(Socket other) {
		for(int i = 0; i < parts.length; i++) {
			if (parts[i] > other.parts[i]) {
				return 1;
			} else if (parts[i] < other.parts[i]) {
				return -1;
			}
		}
		
		if (port > other.port) {
			return 1;
		} else if (port < other.port){
			return -1;
		} else {
			return 0;
		}
	}
	
	public boolean isGreater(Socket other) {
		return compare(other) == 1;
	}

	public boolean isEqual(Socket other) {
		return compare(other) == 0;
	}

	public boolean isLess(Socket other) {
		return compare(other) == -1;
	}
}
