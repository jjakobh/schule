package aufgabe.comparablecontent;

public class Main {
	
	public static void main(String[] args) {
		Socket a = new Socket("255.255.255.255", 443);
		Socket b = new Socket("200.0.0.0", 1337);
		Socket c = new Socket("199.255.255.255", 443);
		Socket d = new Socket("199.255.255.255", 80);

		System.out.println(a.isGreater(b));
		System.out.println(b.isLess(a));
		System.out.println(b.isGreater(c));
		System.out.println(c.isGreater(d));
		System.out.println(d.isEqual(d));
	}
}