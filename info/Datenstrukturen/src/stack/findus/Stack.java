package stack.findus;

public class Stack<T> {
	
	private Papier<T> head;

	public void push(T d) {
		Papier<T> pa = new Papier<T>(d);
		pa.next = head;
		head = pa;
	}
	
	public void pop() {
		if(head == null) return;
		head = head.next;
	}
	
	public Papier<T> top() {
		return head;
	}
	
	public boolean isEmpty() {
		return head == null;
	}
}