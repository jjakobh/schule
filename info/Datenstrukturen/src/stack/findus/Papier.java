package stack.findus;

public class Papier<T> {

	public T data;
	public Papier<T> next;
	
	public Papier(T data) {
		this.data = data;
	}
	
	public String toString() {
		return data.toString();
	}
}
