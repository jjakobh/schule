package stack.findus;

public class Main {
	
	public static String [] aktionen = {
		"Kuchen",
		"Milch",
		"Fahrrad",
		"Schl�ssel",
		"Angel",
		"Leiter",
		"Bullen",
		"Musik"
	};
	
	public static void main(String[] args) {
		Stack<String> s = new Stack<String>();
		for(String aktion : aktionen) {
			s.push(aktion);
		}
		
		while(!s.isEmpty()) {
			System.out.println(s.top());
			s.pop();
		}
	}

	public static void stresstest() {
		Stack<String> s = new Stack<String>();
		
		int i = 0;
		while(true) {
			i++;
			System.out.println(i);
			s.push("kek");
		}
	}
}