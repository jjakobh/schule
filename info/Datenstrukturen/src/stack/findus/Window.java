package stack.findus;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Window extends JFrame {
	private static final long serialVersionUID = -3125232075796045385L;
	
	public Window() {
		
		super("Findus");
		setSize(512, 512);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		this.setLayout(new BorderLayout());

		//
        JPanel container = new JPanel();
        
        //
        JPanel stackPanel = new JPanel();
        stackPanel.add(new JLabel("Stack"));
        
        //
        JPanel controlPanel = new JPanel();
        controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.Y_AXIS));
        controlPanel.add(new JLabel("Control"));
        
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING));
        buttonPanel.add(new JButton("keadsfk"));
        buttonPanel.add(new JButton("kdfek"));
        buttonPanel.add(new JButton("kgek"));
        buttonPanel.add(new JButton("kbek"));
        controlPanel.add(buttonPanel);
        
        //
        container.setLayout(new GridLayout(1,2));
        container.add(stackPanel);
        container.add(controlPanel);

        this.add(container);
		
		
		setVisible(true);
	}
}
