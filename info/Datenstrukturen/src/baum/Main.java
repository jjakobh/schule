package baum;
public class Main {
	
	public static void main(String[] args) {
		BinaryTreeDeluxe<String> tree = 
			new BinaryTreeDeluxe<String>("A", 
				new BinaryTreeDeluxe<String>("B", null, new BinaryTreeDeluxe<String>("C")),
				new BinaryTreeDeluxe<String>("D")
			);
		
		String dot = tree.toDot();
		System.out.println(dot);
	}
}
