package baum;

import nrw.ComparableContent;

public class Entry implements ComparableContent<Entry> {
	
	int value;

	public boolean isGreater(Entry pContent) {
		return this.value > pContent.value;
	}

	public boolean isEqual(Entry pContent) {
		return this.value == pContent.value;
	}

	public boolean isLess(Entry pContent) {
		return this.value < pContent.value;
	}

}