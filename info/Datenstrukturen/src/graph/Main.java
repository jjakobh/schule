package graph;

import nrw.Edge;
import nrw.Vertex;

public class Main {

	public static GraphDeluxe kBaum() {
		GraphDeluxe graph = new GraphDeluxe();
        Vertex k1 = new Vertex("K1");
        Vertex k2 = new Vertex("K2");
        Vertex k3 = new Vertex("K3");
        Vertex k4 = new Vertex("K4");
        Vertex k5 = new Vertex("K5");
        Vertex k6 = new Vertex("K6");
        Vertex k7 = new Vertex("K7");
        Vertex k8 = new Vertex("K8");
        Vertex k9 = new Vertex("K9");
        Vertex k10 = new Vertex("K10");

        graph.addVertex(k1);
        graph.addVertex(k2);
        graph.addVertex(k3);
        graph.addVertex(k4);
        graph.addVertex(k5);
        graph.addVertex(k6);
        graph.addVertex(k7);
        graph.addVertex(k8);
        graph.addVertex(k9);
        graph.addVertex(k10);


        graph.addEdge(new Edge(k1, k6, 19));
        graph.addEdge(new Edge(k1, k5, 78.9));
        graph.addEdge(new Edge(k1, k4, 95.8));
        graph.addEdge(new Edge(k2, k6, 29.2));
        graph.addEdge(new Edge(k2, k10, 65.4));
        graph.addEdge(new Edge(k2, k8, 9.7));
        graph.addEdge(new Edge(k3, k5, 55.1));
        graph.addEdge(new Edge(k5, k6, 95.8));
        graph.addEdge(new Edge(k5, k8, 19));
        graph.addEdge(new Edge(k5, k7, 78.9));
        graph.addEdge(new Edge(k7, k9, 29.2));
        graph.addEdge(new Edge(k8, k9, 9.7));
        graph.addEdge(new Edge(k9, k10, 65.4));

        return graph;
	}
	
	public static GraphDeluxe bigBaum() {
		GraphDeluxe graph = new GraphDeluxe();
		Vertex a = new Vertex("A");
		Vertex b = new Vertex("B");
		Vertex c = new Vertex("C");
		Vertex d = new Vertex("D");
		Vertex e = new Vertex("E");
		Vertex f = new Vertex("F");
		Vertex g = new Vertex("G");
		Vertex h = new Vertex("H");
		Vertex i = new Vertex("I");
		Vertex k = new Vertex("K");
		Vertex l = new Vertex("L");
		Vertex m = new Vertex("M");
		Vertex n = new Vertex("N");
		Vertex o = new Vertex("O");
		Vertex p = new Vertex("P");
		Vertex x = new Vertex("X");
		Vertex y = new Vertex("Y");
		Vertex z = new Vertex("Z");
		
		graph.addVertex(a);
		graph.addVertex(b);
		graph.addVertex(c);
		graph.addVertex(d);
		graph.addVertex(e);
		graph.addVertex(f);
		graph.addVertex(g);
		graph.addVertex(h);
		graph.addVertex(i);
		graph.addVertex(k);
		graph.addVertex(l);
		graph.addVertex(m);
		graph.addVertex(n);
		graph.addVertex(o);
		graph.addVertex(p);
		graph.addVertex(x);
		graph.addVertex(y);
		graph.addVertex(z);
		
		graph.addEdge(new Edge(a, b, 69));
		graph.addEdge(new Edge(a, d, 36));
		graph.addEdge(new Edge(a, m, 36));
		graph.addEdge(new Edge(a, n, 22));
		graph.addEdge(new Edge(b, h, 30));
		graph.addEdge(new Edge(b, z, 32));
		graph.addEdge(new Edge(b, d, 64));
		graph.addEdge(new Edge(b, i, 34));
		graph.addEdge(new Edge(b, x, 26));
		graph.addEdge(new Edge(c, i, 40));
		graph.addEdge(new Edge(c, x, 23));
		graph.addEdge(new Edge(c, m, 31));
		graph.addEdge(new Edge(d, l, 95));
		graph.addEdge(new Edge(d, n, 20));
		graph.addEdge(new Edge(e, g, 60));
		graph.addEdge(new Edge(e, k, 31));
		graph.addEdge(new Edge(e, f, 79));
		graph.addEdge(new Edge(e, o, 102));
		graph.addEdge(new Edge(f, k, 29));
		graph.addEdge(new Edge(f, p, 57));
		graph.addEdge(new Edge(f, o, 14));
		graph.addEdge(new Edge(g, l, 58));
		graph.addEdge(new Edge(g, y, 30));
		graph.addEdge(new Edge(g, k, 58));
		graph.addEdge(new Edge(h, y, 29));
		graph.addEdge(new Edge(h, k, 31));
		graph.addEdge(new Edge(h, i, 65));
		graph.addEdge(new Edge(h, p, 34));
		graph.addEdge(new Edge(i, m, 43));
		graph.addEdge(new Edge(i, p, 55));
		graph.addEdge(new Edge(k, y, 20));
		graph.addEdge(new Edge(k, p, 25));
		graph.addEdge(new Edge(l, z, 40));
		graph.addEdge(new Edge(l, h, 106));
		graph.addEdge(new Edge(m, x, 12));
		graph.addEdge(new Edge(n, z, 30));
		graph.addEdge(new Edge(n, x, 29));
		graph.addEdge(new Edge(o, p, 91));
		graph.addEdge(new Edge(y, z, 23));
		return graph;
	}
	
	public static void main(String[] args) {
		GraphDeluxe graph = bigBaum();
		GraphDeluxe spannBaum = graph.kruskal();
		System.out.println(spannBaum.findEdgeSumme());
	}

}
