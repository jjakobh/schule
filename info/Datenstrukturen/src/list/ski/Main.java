package list.ski;

public class Main {

	public static void main(String[] args) {
		Fahrer[] fahrer = {
				new Fahrer("Hargen", 46.61),
				new Fahrer("Kirscher", 46.79),
				new Fahrer("Dopfer", 47.48),
				new Fahrer("Christophersen", 46.79),
				new Fahrer("Kuroschilov", 47.21),
				new Fahrer("Stefanogros", 48.78),
				new Fahrer("Neurolter", 48.52),
				new Fahrer("Myrer", 47.66),
				new Fahrer("Grange", /*48.56*/46.79),
				new Fahrer("Ratzolin", 47.28)
		};
		
		List list = new List();
		for(Fahrer f : fahrer) {
			list.insertSorted(f);
		}
		
		list.remove("hArGEn", "dopfer", "ratzolin"); // miese doper
		list.print();
	}
}
