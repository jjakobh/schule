package list.ski;

public class Knoten {
	private Fahrer data;
	private Knoten next;
	
	public Knoten(Fahrer data) {
		this.data = data;
	}
	public Knoten(Fahrer data, Knoten next) {
		this.data = data;
		this.next = next;
	}
	
	public Fahrer getData() { return this.data; }
	public Knoten getNext() { return this.next; }
	public void setData(Fahrer data) { this.data = data; }
	public void setNext(Knoten next) { this.next = next; }
}
