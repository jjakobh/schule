package list.ski;

public class List {
	private Knoten anchor;
	
	public List() {
		anchor = new Knoten(null);
	}
	
	private Knoten getLast() {
		Knoten tmp = anchor;
		while(tmp.getNext() != null) {
			tmp = tmp.getNext();
		}
		return tmp;
	}
	
	public void insert(Fahrer fahrer) {
		getLast().setNext(new Knoten(fahrer));
	}

	// anchor -> f1 -> f2
	public void insertSorted(Fahrer fahrer) {
		for (Knoten tmp = anchor; tmp != null; tmp = tmp.getNext()) {
			if(tmp.getNext() == null) {
				// ist letztes element, also füge hinten an
				tmp.setNext(new Knoten(fahrer));
				return;
			}
			else if (tmp.getNext().getData().getTime() >= fahrer.getTime()) {
				// vorher: tmp -> next
				// nacher: tmp -> neu -> next
				Knoten neu = new Knoten(fahrer, tmp.getNext());
				tmp.setNext(neu);
				return;
			}
		}
		// BUG des Tages...... NullPointerException !!!! >:(
	}
	
	@SuppressWarnings("unused")
	private void printDump() {
		for (Knoten tmp = anchor.getNext(); tmp != null; tmp = tmp.getNext()) {
			System.out.println(tmp.getData());
		}
	}
	
	public void print() {
		printKLUK();
	}

	private void printKLUK() {
		double lastVal = -1;
		int platz = 0;
		int diff = 0;
		boolean lastWasSame = false;
		for (Knoten tmp = anchor.getNext(); tmp != null; tmp = tmp.getNext()) {
			boolean same = lastVal == tmp.getData().getTime();
			
			if(!same) {
				platz++;
			}else{
				diff++;
				lastWasSame = true;
			}
			if (!same && lastWasSame) {
				lastWasSame = false;
				platz += diff;
			}
			System.out.println(String.format("[%d]\t%s", platz, tmp.getData()));
			
			lastVal = tmp.getData().getTime();
		}
	}
	
	public void remove(String nameToRemove) {
		Knoten prev = anchor;
		Knoten tmp = anchor.getNext();
		while(tmp != null) {
			
			if(tmp.getData().getName().equalsIgnoreCase(nameToRemove)) {
				prev.setNext(tmp.getNext());
			}
			
			tmp = tmp.getNext();
			prev = prev.getNext();
		}
	}
	
	public void remove(String... names){
		for (String n : names) {
			remove(n);
		}
	}

	
}
