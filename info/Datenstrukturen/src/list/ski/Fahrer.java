package list.ski;

public class Fahrer {
	private String name;
	private double time;
	
	public Fahrer(String name, double d) {
		this.name = name;
		this.time = d;
	}
	
	public String getName() {
		return name;
	}
	public double getTime() {
		return time;
	}
	
	public String toString() {
		return time + " -> " + name;
	}
}
