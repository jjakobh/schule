package arrays.zahlenspeicher;

public class Zahlenspeicher {

	public int[] zahlen;
	
	public Zahlenspeicher(int n, int m){
		zahlen = new int[n];
		for (int i = 0; i < n; i++) {
			zahlen[i] = (int) (1+Math.random()*m);
		}
	}
	
	public void druckeDich(){
		String text = "";
		for (int i = 0; i < zahlen.length; i++){
			text += zahlen[i];
			if (i != zahlen.length-1)
				text += ",";
		}
		System.out.println(text);
	}
	
	public int summe(){
		int sum = 0;
		for (int i = 0; i < zahlen.length; i++){
			sum += zahlen[i];
		}
		return sum;
	}
	
	public int minimum(){
		int min = 10000000;
		for (int i = 0; i < zahlen.length; i++){
			if(zahlen[i] < min){
				min = zahlen[i];
			}
		}
		return min;
	}
	
	public double durchschnitt(){
		return summe()/(double)zahlen.length;
	}
	
}
