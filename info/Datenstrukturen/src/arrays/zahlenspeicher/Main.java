package arrays.zahlenspeicher;

public class Main {

	public static void main(String[] args) {
		Zahlenspeicher z = new Zahlenspeicher(6, 420);
		
		z.druckeDich();
		System.out.println("Durchschnitt: "+z.durchschnitt());
		System.out.println("Minimum: "+z.minimum());
	}

}
