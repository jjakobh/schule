package arrays.einwohner;

public class Mensch {

	private int id;
	private String name;
	
	public Mensch(int id, String newName){
		this.id = id;
		setName(newName);
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString(){
		return "{id="+id+",name="+name+"}";
	}


	
}
