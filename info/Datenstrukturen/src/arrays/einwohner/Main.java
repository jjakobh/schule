package arrays.einwohner;

public class Main {
	
	private static final int einwohnerZahl = 1000000;
	private static String[] namen = {"Gustav","Karl", "Klaus", "Hans", "Furkan", "Foo", "Frikadella", "Mathilde", "Bertha", "Gundule", "Brunhilde", "Nikolaj", "Santa"};

	public static void main(String[] args) {

		Mensch[] einwohner = new Mensch[einwohnerZahl];
		for (int i = 0; i < einwohnerZahl; i++){
			String name = namen[(int)(Math.random()*namen.length)];
			einwohner[i] = new Mensch(i, name);
		}
		
		for(int i = 0; i < einwohnerZahl; i++){
			System.out.println(einwohner[i]);
		}
	}

}
