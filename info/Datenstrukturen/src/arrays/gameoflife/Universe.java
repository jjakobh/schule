package arrays.gameoflife;

import java.awt.Color;

import javax.swing.JPanel;

public class Universe {
	
	public boolean[][] feld;
	public int[][] age;
	private JPanel[][] panels;
	
	public Universe(int x, int y){
		feld = new boolean[x][y];
		age = new int[x][y];
	}
	
	public Universe(int x, int y, double percent){
		feld = new boolean[x][y];
		age = new int[x][y];
		randomize(percent);
	}
	
	public void randomize(double percent) {
		for (int i = 0; i < feld.length; i++){
			for(int j = 0; j < feld[i].length; j++){
				feld[i][j] = Math.random() < percent;
			}
		}
	}
	
	public void setPanelArray(JPanel[][] arr) {
		panels = arr;
	}
	
	private void draw(int x, int y, boolean value) {
		if(panels != null){
			Color c = new Color(Color.HSBtoRGB(age[x][y]/100f-0.5f, 1, 1));
			//Color c = new Color(Math.min(255, age[x][y]*5), 0, 0);
			panels[x][y].setBackground(value?c:Color.WHITE);
		}
	}
	public void redraw() {
		if(panels == null) return;
		for (int x = 0; x < feld.length; x++) {
			for (int y = 0; y < feld[0].length; y++) {
				draw(x, y, feld[x][y]);
			}
		}
	}
	
	public void spawn(Shape shape, boolean rotate, int x, int y) {
		
		int platzRechts = feld.length - (x+shape.getWidth());
		if( platzRechts < 1 )
			x = feld.length - (shape.getWidth()+1);
	
		int platzUnten = feld[0].length - (y+shape.getHeight());
		if( platzUnten < 1 )
			y = feld[0].length - (shape.getHeight()+1);
		
		int[][] points = shape.getPoints();
		for(int i = 0; i < points.length; i++) {
			int xx, yy;
			if(rotate){
				xx = x+points[i][0];
				yy = y+points[i][1];
			}else{
				xx = x+points[i][1];
				yy = y+points[i][0];
			}
			
			feld[xx][yy] = true;
			draw(xx, yy, true);
		}
	}
	
	public void print(){
		for(int y = 0; y < feld[0].length; y++){
			String row = "";
			for(int x = 0; x < feld.length; x++){
				if(feld[x][y]){
					row += "x ";
				}else{
					row += ". ";
				}
			}
			System.out.println(row);
		}
	}
	
	private boolean[][] copyFeld(){
		boolean[][] newFeld = new boolean[feld.length][feld[0].length];
		for(int x = 0; x < feld.length; x++){
			for(int y = 0; y < feld[0].length; y++){
				newFeld[x][y] = feld[x][y];
			}
		}
		return newFeld;
	}
	
	public void iterate(){
		boolean[][] newFeld = copyFeld();

		for(int x = 0; x < feld.length; x++){
			for(int y = 0; y < feld[0].length; y++){
				int nc = getNeighbourCount(x, y);
				if (nc < 2 || nc > 3) {
					newFeld[x][y] = false;
				}else if (nc == 3) {
					newFeld[x][y] = true;
				}
				
				if(feld[x][y]){
					if(newFeld[x][y]) age[x][y]++;
					else age[x][y] = 0;
				}else{
					if(newFeld[x][y]) age[x][y] = 1;
				}
				
				if(feld[x][y] != newFeld[x][y] || newFeld[x][y]){
					draw(x, y, newFeld[x][y]);
				}
			}
		}
		
		feld = newFeld;
	}
	
	private int getNeighbourCount(int x, int y){
		int neighbourCount = 0;
		for (int i = -1; i <= 1; i++){
			for (int j = -1; j <=1; j++){
				if (i == 0 &&  j == 0)
					continue;
				int newx = x+i;
				int newy = y+j;
				if (newx >= 0 && newx < feld.length && newy >= 0 && newy < feld[0].length){
					if (feld[newx][newy])
						neighbourCount++;
				}
			}
		}
		return neighbourCount;
	}
}
