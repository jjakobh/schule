package arrays.gameoflife;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Window extends JFrame {

	private static final long serialVersionUID = -959574094618570128L;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				new Window(600, 700, 100, 100).setVisible(true);
			}
		});
	}

	public Window(int width, int height, int rows, int cols) {
		setSize(width, height);
		setLocationRelativeTo(null);

		JPanel guiPanel = new JPanel();

		setDefaultCloseOperation(EXIT_ON_CLOSE);

		JPanel[][] jpanels = new JPanel[rows][cols];
		JPanel gamePanel = new JPanel(new GridLayout(rows, cols));
		gamePanel.setPreferredSize(new Dimension(getSize().width, getSize().height-100));

		for (int c = 0; c < cols; c++) {
			for (int r = 0; r < rows; r++) {
				JPanel p = new JPanel();
				p.setBackground(Color.WHITE);
				p.putClientProperty("x", r);
				p.putClientProperty("y", c);
				jpanels[r][c] = p;
				gamePanel.add(p);
			}
		}

		JPanel buttonPanel = new JPanel(new FlowLayout());
		final JButton playPauseBtn = new JButton("Play");
		JButton iterateBtn = new JButton("Next");
		JButton generateBtn = new JButton("Randomize");
		final JComboBox<String> shapeSelect = new JComboBox<String>(Shape.shapes);
		final JCheckBox rotateCheck = new JCheckBox();
		JSlider speedSlider = new JSlider(0, 500, 0);
		

		buttonPanel.add(playPauseBtn);
		buttonPanel.add(iterateBtn);
		buttonPanel.add(generateBtn);
		buttonPanel.add(shapeSelect);
		buttonPanel.add(rotateCheck);
		buttonPanel.add(speedSlider);
		
		guiPanel.add(gamePanel);
		guiPanel.add(buttonPanel);
		add(guiPanel);

		final Universe u = new Universe(rows, cols);
		u.setPanelArray(jpanels);
		u.redraw();

		final Timer timer = new Timer(speedSlider.getValue(),
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						u.iterate();
					}
				});

		JPanel test = new JPanel(new GridLayout(2, 2));
		for (int i = 0; i < 4; i++) {
			test.add(new JPanel());
		}

		playPauseBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(playPauseBtn.getText() == "Play"){
					timer.start();
					playPauseBtn.setText("Pause");
				}else{
					timer.stop();
					playPauseBtn.setText("Play");
				}
			}
		});
		iterateBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				u.iterate();
			}
		});
		generateBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				u.randomize(.5);
				u.redraw();
			}
		});
		speedSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent event) {
				timer.setDelay(((JSlider) event.getSource()).getValue());
			}
		});

		gamePanel.addMouseListener(new MouseListener() {
			@Override public void mouseReleased(MouseEvent e) {}
			@Override public void mouseExited(MouseEvent e) {}
			@Override public void mouseEntered(MouseEvent e) {}
			@Override public void mouseClicked(MouseEvent e) {}
			
			@Override public void mousePressed(MouseEvent e) {
				JPanel p = (JPanel) e.getComponent().getComponentAt(e.getPoint());
				u.spawn(Shape.getShape((String) shapeSelect.getSelectedItem()), rotateCheck.isSelected(), (int) p.getClientProperty("x"), (int) p.getClientProperty("y"));
			}
		});

	}

}
