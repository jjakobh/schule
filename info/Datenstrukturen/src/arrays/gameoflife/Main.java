package arrays.gameoflife;

import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		//Universe u = new Universe(12, 6, 0.5);
		Universe u = new Universe(12, 6, 0.5);
		while(true){
			System.out.println();
			u.print();
			
			try {
				System.in.read();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			u.iterate();
		}
		
	}
	

}
