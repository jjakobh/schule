package arrays.gameoflife;

public class Shape {

	private int width, height;
	private int[][] shape;

	public Shape(int[][] values) {
		shape = values;
		int maxX = -100;
		int maxY = -100;
		for (int i = 0; i < values.length; i++) {
			if (values[i][0] > maxX)
				maxX = values[i][0];
			if (values[i][1] > maxY)
				maxY = values[i][1];
		}
		width = maxX;
		height = maxY;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int[][] getPoints() {
		return shape;
	}

	public static final String[] shapes = {"glider", "glidercannon"};

	
	private static int[][] glider = { { 0, 0 }, { 0, 1 }, { 0, 2 }, { 1, 0 }, { 2, 1 } };

	private static int[][] glidercannon = { { 1, 5 }, { 2, 5 }, { 1, 6 },
			{ 2, 6 }, { 11, 5 }, { 11, 6 }, { 11, 7 }, { 12, 4 }, { 13, 3 },
			{ 14, 3 }, { 12, 8 }, { 13, 9 }, { 14, 9 }, { 15, 6 }, { 16, 4 },
			{ 17, 5 }, { 17, 6 }, { 17, 7 }, { 18, 6 }, { 16, 8 }, { 21, 3 },
			{ 21, 4 }, { 21, 5 }, { 22, 3 }, { 22, 4 }, { 22, 5 }, { 23, 2 },
			{ 23, 6 }, { 25, 1 }, { 25, 2 }, { 25, 6 }, { 25, 7 }, { 35, 3 },
			{ 35, 4 }, { 36, 3 }, { 36, 4 } };

	public static Shape getGlider() {
		return new Shape(glider);
	}

	public static Shape getGliderCannon() {
		return new Shape(glidercannon);
	}

	public static Shape getShape(String selectedItem) {
		switch (selectedItem) {
		case "glider":
			return new Shape(glider);
		case "glidercannon":
			return new Shape(glidercannon);
		default:
			return new Shape(glider);
		}
	}
}
